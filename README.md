# README #

The objective of this application is to download Tablo&tm; recording segments for the purpose of (manually) removing commercials and merging segments into a single video file.

### Process Steps ###

* Select a recording and download the recoreding segments by clicking on the button labeled *Donwload*.  This process downloads all the segments and generates an image with the first frame of each video segment.
* When the download process is complete, double-click on the recording or click on an entry in the *Pre-Download Monitor* panel entry to open the segment viewer.  The download progress will be displayed on the progress panel of the main window. 
* When the segment viewer finishes loading (may take a while for large recordings), click segment tiles to remove.  All tiles are selected by default (green borders). Clicking on a tile toggles the selection state.  Tiles with a red border are **removed**.  
* A group of tiles can be selected at once by selecting the first tile, then selecting the last tile while pressing the SHIFT key.  Additionally tiles can be selected or unselected by using the LEFT and RIGHT arrows to navigate and pressing the SPACE bar to toggle the tile selection.
* An attempt is made to automatically set offsets for commercial start and end position--for those tiles with commercial break occuring in the middle of the tile.  Alternatively, press 'S' or 'E' to attempt to the commercial starting or ending position for a while.
* The commercial break boundary (start or finish) is determined by completely black frames.  Some shows may not have enough black frames (or none at all) to signal a commercial boundary.  In such cases, it is possible to zoom into the tile by pressing 'Z' and manually selecting the commercial frames within the tile.  
* Select a tile and press 'S' to indicate the commercial STARTS on that tile or 'E' to indicate the commercial ENDS on that frame--the commercial frames will be selected automatcially.
* Having selected all the tiles to remove, click on the button labeled **Merge**.  The Segment Tile Viewer may be closed.  The merge progress will be displayed on the progress monitor pane of the main window.  The recording will be saved in your user's **Videos** directory (e.g. /home/dixsonhill/Videos).

### Navigating the Main Window ###

* Click on the button labeled **Reload** to reload the recordings from the local cache.
* Click on the button labeled **Sync** to Synchronize the local Cache--removes local recording metadata files that have been removed from the Tablo DVR unit, and re-downloads all recording metadata.
* Click on the button labeled **Segments** to open the Segment tile viewer for the selected recording (equivalent to double-clicking on a recording).

## Local Cache ###
Application information is written do a directory called **.tablo-recordings** in your user's home directory.  Application settings are located in a file called **settings.properties** in the **config** sub-directory; update these settings to change application behavior


### FFMPEG ###
The application requires the FFMPEG (https://www.ffmpeg.org/) to perform video processing operations.  If FFMPEG is not downloaded and configured automatically, downloaded manually and update the path in the setting **general.videoCommand**.
